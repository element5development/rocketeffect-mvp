<?php 
/*----------------------------------------------------------------*\

	ERROR / NO PAGE FOUND

\*----------------------------------------------------------------*/
?>


<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>

<main>
	<article>
		<section class="error">
			<h2>This page seems to have been lost in a black hole.</h2>
			<div class="icon">
				<img src="<?php echo get_template_directory_uri();  ?>/dist/images/404-hole.svg" alt="" />
				<img src="<?php echo get_template_directory_uri();  ?>/dist/images/404-stars.svg" alt="" />
			</div>
			<a href="<?php echo get_home_url(); ?>" class="button is-purple">take me back home.</a>
		</section>
	</article>
</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>