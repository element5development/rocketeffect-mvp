<?php 
/*----------------------------------------------------------------*\

	PRIMARY NAVIGATION
	Most commonly contains all the top level pages and search options.

\*----------------------------------------------------------------*/
?>

<nav class="navigation-block">
	<a href="<?php echo get_home_url(); ?>">
		<svg>
			<use xlink:href="#logo-white" />
		</svg>
	</a>
	<a href="<?php the_permalink(308); ?>" class="button is-ghost is-white smooth-scroll">
		<span>Request a</span> Demo
	</a>
</nav>