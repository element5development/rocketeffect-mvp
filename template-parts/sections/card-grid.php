<?php 
/*----------------------------------------------------------------*\

	SLIDER FOR ICONS 
	commonly used for awards and partners

\*----------------------------------------------------------------*/
?>
<?php if( have_rows('cards') ): ?>
	<section class="cards">
		<div>
			<?php while ( have_rows('cards') ) : the_row(); ?>
				<div class="card">
					<h3><?php the_sub_field('title'); ?></h3>
					<p><?php the_sub_field('description'); ?></p>
					<?php $link = get_sub_field('button'); ?>
					<a class="button is-borderless is-ghost is-purple" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>"><?php echo $link['title']; ?></a>
				</div>
			<?php endwhile; ?>
		</div>
	</section>
<?php endif; ?>