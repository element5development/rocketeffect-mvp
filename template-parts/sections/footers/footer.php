<?php 
/*----------------------------------------------------------------*\

	DEFAULT FOOTER
	Made up of widget areas
	May need to add or remove additional areas depending on the design

\*----------------------------------------------------------------*/
?>

<footer>
	<div>
		<a href="<?php echo get_home_url(); ?>">
			<svg>
				<use xlink:href="#logo-red" />
			</svg>
		</a>
		<section>
			<a href="<?php the_permalink(303) ?>" class="button is-borderless is-ghost is-large is-purple">
				<?php 
					if ( get_field('page_title', 303) ) :
						the_field('page_title', 303);
					else :
						echo get_the_title(303);
					endif;
				?>
			</a>
			<nav>
				<ul>
					<li>
						<a href="tel:+12485341228">US: +1 (248) 534-1228</a>
					</li>
					<li>
						<a href="tel:+447449642102">EU: +44 7449 642 102</a>
					</li>
				</ul>
			</nav>
			<p>32A, Hlybochytska St, Ste 151, Kyiv 04050, Ukraine</p>
			<p>412 E 4th Street Royal Oak, MI	48067, United States</p>
		</section>
		<section>
			<a href="<?php the_permalink(300) ?>" class="button is-borderless is-ghost is-large is-purple">
				<?php 
					if ( get_field('page_title', 300) ) :
						the_field('page_title', 300);
					else :
						echo get_the_title(300);
					endif;
				?>
			</a>
			<nav>
				<?php wp_nav_menu(array( 'theme_location' => 'legal_nav' )); ?>
			</nav>
			<p>© Copyright <?php echo date('Y'); ?> <?php echo get_bloginfo( 'name' ); ?>. All rights reserved.</p>
		</section>
	</div>
</footer>