<?php 
/*----------------------------------------------------------------*\

	HEADER WITH IMAGE BACKGROUND

\*----------------------------------------------------------------*/
?>

<header class="page-title has-image">
	<section>

		<h1>
			<?php 
				if ( get_field('page_title') ) :
					the_field('page_title');
				elseif ( is_404() ) :
					echo 'Page Not Found';
				else :
					the_title();
				endif;
			?>
		</h1>

		<?php if ( get_field('title_description') ) : ?>
			<p>
				<?php the_field('title_description'); ?>
			</p>
		<?php endif; ?>

	</section>

</header>