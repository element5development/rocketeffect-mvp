var $ = jQuery;

$(document).ready(function () {
	/*----------------------------------------------------------------*\
  	ENTRANCE ANIMATIONS
	\*----------------------------------------------------------------*/
	emergence.init({
		reset: false,
		offsetTop: 20,
		offsetRight: 20,
		offsetBottom: 20,
		offsetLeft: 20,
	});
	/*----------------------------------------------------------------*\
  	STICKY NAV
	\*----------------------------------------------------------------*/
	function defaultnav() {
		$('.navigation-block').removeClass('is-fixed').css('top', 'auto');
	}
	$(window).scroll(function () {
		var scroll = $(window).scrollTop();

		if (scroll >= 500) {
			$('.navigation-block').addClass('is-fixed').css('top', '0px');
		} else if (scroll > 200 && scroll < 500) {
			$('.navigation-block').css('top', '-500px');
		} else {
			if (scroll < position) {
				$('.navigation-block').css('top', '-500px');
				setTimeout(defaultnav, 1000);
			}
			var position = scroll;
			$('.navigation-block').removeClass('is-fixed').css('top', 'auto');
		}
	});
	/*----------------------------------------------------------------*\
		FILE UPLOAD
	\*----------------------------------------------------------------*/
	if ($('input[type="file"]').length > 0) {
		var fileInput = document.querySelector("input[type='file']");
		var button = document.querySelector("input[type='file']+span");
		fileInput.addEventListener("change", function (event) {
			button.innerHTML = this.value;
			fileInput.classList.add("file-uploaded");
		});
	}
	/*----------------------------------------------------------------*\
		ICON SLIDER
	\*----------------------------------------------------------------*/
	$('.icons').slick({
		arrows: false,
		infinite: true,
		slidesToShow: 6,
		slidesToScroll: 3,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive: [{
				breakpoint: 800,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	});

	$('.client-icons').slick({
		arrows: false,
		infinite: true,
		slidesToShow: 5,
		slidesToScroll: 2,
		autoplay: true,
		autoplaySpeed: 3000,
		responsive: [{
				breakpoint: 800,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
				}
			},
			{
				breakpoint: 500,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
				}
			}
		]
	});
	/*----------------------------------------------------------------*\
		HOME FORM FLIP
	\*----------------------------------------------------------------*/
	var name = $("#input_6_1").val();
	$("#input_6_1").on("change", function () {
		name = $(this).val()
		$(this).attr("value", name);
	});
	var cname = $("#input_6_2").val();
	$("#input_6_2").on("change", function () {
		cname = $(this).val()
		$(this).attr("value", cname);
	});
	var site = $("#input_6_6").val();
	$("#input_6_6").on("change", function () {
		site = $(this).val()
		$(this).attr("value", site);
	});
	$('.flip-container .front button').click(function () {
		if ($('#input_6_1').hasClass("LV_valid_field") && $('#input_6_2').hasClass("LV_valid_field") && $('#input_6_6').hasClass("LV_valid_field")) {
			var name = $("#input_6_1").attr('value');
			$("#input_8_7").val(name);
			var cname = $("#input_6_2").attr('value');
			$("#input_8_8").val(cname);
			var site = $("#input_6_6").attr('value');
			$("#input_8_6").val(site);
			$('.flip-container').addClass('flip');
		}
	});
	$('.flip-container .back button').click(function () {
		$('.flip-container').addClass('success').removeClass('flip');
	});
	/*----------------------------------------------------------------*\
		HOME FORM PARALLAX
	\*----------------------------------------------------------------*/
	var rellax = new Rellax('.home .page-title .title', {
		speed: -4,
		center: false,
		wrapper: null,
		round: true,
		vertical: true,
		horizontal: false
	});
	if ($(window).width() < 800) {
		rellax.destroy();
	}
	/*----------------------------------------------------------------*\
		HOME GRAPHIC PARALLAX
	\*----------------------------------------------------------------*/
	var rellax = new Rellax('.home .graphic', {
		speed: -4,
		center: false,
		wrapper: null,
		round: true,
		vertical: true,
		horizontal: false
	});
	if ($(window).width() < 800) {
		rellax.destroy();
	}

});