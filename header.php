<?php 
/*----------------------------------------------------------------*\

	HTML HEAD CONTENT
	Commonly contains site meta data and tracking scripts.
	External resources are not referanced here, refer to the functions.php

\*----------------------------------------------------------------*/
?>

<!DOCTYPE html>

<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-126144991-1"></script>
	<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());

	gtag('config', 'UA-126144991-1');
	</script>
	<!-------------------------->
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title>
		<?php wp_title(''); ?>
	</title>
	<?php wp_head(); ?>
	<!-- Hotjar Tracking Code for https://www.rocketeffect.com/ -->
	<script>
		(function(h,o,t,j,a,r){
				h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
				h._hjSettings={hjid:1023349,hjsv:6};
				a=o.getElementsByTagName('head')[0];
				r=o.createElement('script');r.async=1;
				r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
				a.appendChild(r);
		})(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
	</script>
	<!-------------------------->
</head>

<body <?php body_class(); ?>>

	<?php get_template_part('template-parts/icon-set'); ?>