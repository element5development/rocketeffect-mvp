<?php 
/*----------------------------------------------------------------*\

	Template Name: Form
	
\*----------------------------------------------------------------*/
?>

<?php get_header(); ?>

<?php get_template_part('template-parts/elements/navigation'); ?>

<?php get_template_part('template-parts/sections/headers/header'); ?>


<main>

	<article>
			<section class="form">
				<?php if ( get_field('heading') ) : ?>
					<h2><?php the_field('heading'); ?></h2>
				<?php endif; ?>
				<?php if ( get_field('form_description') ) : ?>
					<p><?php the_field('form_description'); ?></p>
				<?php endif; ?>
				<?php echo do_shortcode('[gravityform id="'.get_field('form_id').'" title="false" description="false"]') ?>
			</section>
	</article>

</main>

<?php get_template_part('template-parts/sections/footers/footer'); ?>

<?php get_footer(); ?>